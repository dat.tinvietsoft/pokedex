import * as React from "react";
import Svg, { G, Path, Defs, ClipPath } from "react-native-svg";

const Normal = (props) => {
  return (
    <Svg
      width={25}
      height={25}
      viewBox="0 0 25 25"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <G clipPath="url(#clip0_347_101)">
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M25 12.5C25 19.404 19.404 25 12.5 25S0 19.404 0 12.5 5.596 0 12.5 0 25 5.596 25 12.5zm-5.357 0a7.143 7.143 0 11-14.286 0 7.143 7.143 0 0114.286 0z"
          fill={`${props.color ? props.color : "#17171B"}`}
        />
      </G>
      <Defs>
        <ClipPath id="clip0_347_101">
          <Path fill="#fff" d="M0 0H25V25H0z" />
        </ClipPath>
      </Defs>
    </Svg>
  );
};

export default Normal;
