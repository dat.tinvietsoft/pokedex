import * as React from "react";
import Svg, { G, Path, Defs, ClipPath } from "react-native-svg";

const Fairy = (props) => {
  return (
    <Svg
      width={25}
      height={25}
      viewBox="0 0 25 25"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <G clipPath="url(#clip0_347_77)">
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M5.016 19.824l4.01-1.163 3.464 6.333a.012.012 0 00.02 0l3.464-6.333 4.01 1.163a.012.012 0 00.015-.015l-1.163-3.93 6.158-3.369a.012.012 0 000-.02l-6.21-3.396L20 4.992a.012.012 0 00-.015-.015l-4.104 1.19-3.37-6.16a.012.012 0 00-.02 0l-3.37 6.16-4.104-1.19a.012.012 0 00-.015.015l1.214 4.102L.006 12.49a.012.012 0 000 .02l6.158 3.368-1.163 3.931a.012.012 0 00.015.015zm3.112-7.28l2.84 1.553 1.554 2.84a.012.012 0 00.021 0l1.554-2.84 2.84-1.554a.012.012 0 000-.02l-2.84-1.554-1.554-2.841a.012.012 0 00-.02 0l-1.554 2.84-2.841 1.554a.012.012 0 000 .021z"
          fill={`${props.color ? props.color : "#17171B"}`}
        />
      </G>
      <Defs>
        <ClipPath id="clip0_347_77">
          <Path fill="#fff" d="M0 0H25V25H0z" />
        </ClipPath>
      </Defs>
    </Svg>
  );
};

export default Fairy;
