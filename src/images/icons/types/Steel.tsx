import * as React from "react";
import Svg, { Path } from "react-native-svg";

const Steel = (props) => {
  return (
    <Svg
      width={25}
      height={25}
      viewBox="0 0 25 25"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M.002 12.515a.018.018 0 010-.018L6.29 1.757a.018.018 0 01.016-.01h12.46c.006 0 .012.004.015.01l6.217 10.74a.018.018 0 010 .018L18.78 23.244a.018.018 0 01-.016.009H6.304a.018.018 0 01-.015-.01L.002 12.516zm18.29-.015a5.79 5.79 0 11-11.58 0 5.79 5.79 0 0111.58 0z"
        fill={`${props.color ? props.color : "#17171B"}`}
      />
    </Svg>
  );
};

export default Steel;
