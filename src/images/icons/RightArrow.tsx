import * as React from "react";
import Svg, { Path } from "react-native-svg";

const RightArrow = (props) => {
  return (
    <Svg
      width={20}
      height={21}
      viewBox="0 0 20 21"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M1.283 11.777H15.62L9.357 18.04c-.5.5-.5 1.322 0 1.823a1.278 1.278 0 001.81 0l8.458-8.458a1.279 1.279 0 000-1.81l-8.459-8.458a1.28 1.28 0 10-1.81 1.81L15.62 9.21H1.283C.578 9.21 0 9.787 0 10.493s.578 1.284 1.283 1.284z"
        fill={`${props.color ? props.color : "#17171B"}`}
      />
    </Svg>
  );
};

export default RightArrow;
