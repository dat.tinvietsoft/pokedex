import * as React from "react";
import Svg, { Path } from "react-native-svg";

const Search = (props) => {
  return (
    <Svg
      width={20}
      height={20}
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M12.188 13.623a6.421 6.421 0 111.436-1.435c.1.05.195.117.279.201l3.784 3.784a1.07 1.07 0 11-1.514 1.513l-3.784-3.783a1.065 1.065 0 01-.201-.28zm.514-5.201a4.28 4.28 0 11-8.561 0 4.28 4.28 0 018.562 0z"
        fill={`${props.color ? props.color : "#17171B"}`}
      />
    </Svg>
  );
};

export default Search;
