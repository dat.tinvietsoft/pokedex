import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

const initialState = {
  value: 0,
  status: "idle",
};

export const pokeSlice = createSlice({
  name: "poke",
  initialState,
  reducers: {},
  extraReducers: (builder) => {},
});

export default pokeSlice.reducer;
