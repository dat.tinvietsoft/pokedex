export interface Ability {
  ability: {
    name: string;
    url: string;
  };
  is_hidden: boolean;
  slot: number;
}

export interface Form {
  name: string;
  url: string;
}

export interface GameIndex {
  game_index: number;
  version: {
    name: string;
    url: string;
  };
}

export interface HeldItem {
  item: {
    name: string;
    url: string;
  };
  version_details: {
    rarity: number;
    version: {
      name: string;
      url: string;
    };
  }[];
}

export interface Move {
  move: {
    name: string;
    url: string;
  };
  version_group_details: {
    level_learned_at: number;
    move_learn_method: {
      name: string;
      url: string;
    };
    version_group: {
      name: string;
      url: string;
    };
  }[];
}

export interface PastType {
  generation: {
    name: string;
    url: string;
  };
  types: {
    slot: number;
    type: {
      name: string;
      url: string;
    };
  }[];
}

export interface SpriteImage {
  back_default?: string | null;
  back_gray?: string | null;
  back_transparent?: string | null;
  back_shiny?: string | null;
  back_shiny_transparent?: string | null;
  back_female?: string | null;
  back_shiny_female?: string | null;
  front_default?: string | null;
  front_gray?: string | null;
  front_transparent?: string | null;
  front_shiny?: string | null;
  front_shiny_transparent?: string | null;
  front_female?: string | null;
  front_shiny_female?: string | null;
  animated?: {
    back_default?: string | null;
    back_female?: string | null;
    back_shiny?: string | null;
    back_shiny_female?: string | null;
    front_default?: string | null;
    front_female?: string | null;
    front_shiny?: string | null;
    front_shiny_female?: string | null;
  };
}

export interface SpriteOther {
  dream_world: {
    front_default: string | null;
    front_female: string | null;
  };
  home: {
    front_default: string | null;
    front_female: string | null;
    front_shiny: string | null;
    front_shiny_female: string | null;
  };
  "official-artwork": {
    front_default: string | null;
    front_shiny: string | null;
  };
}

export interface SpriteVersion {
  "generation-i": {
    "red-blue": SpriteImage;
    yellow: SpriteImage;
  };
  "generation-ii": {
    crystal: SpriteImage;
    gold: SpriteImage;
    silver: SpriteImage;
  };
  "generation-iii": {
    emerald: SpriteImage;
    "firered-leafgreen": SpriteImage;
    "ruby-sapphire": SpriteImage;
  };
  "generation-iv": {
    "diamond-pearl": SpriteImage;
    "heartgold-soulsilver": SpriteImage;
    platinum: SpriteImage;
  };
  "generation-v": {
    "black-white": SpriteImage;
  };
  "generation-vi": {
    "omegaruby-alphasapphire": SpriteImage;
    "x-y": SpriteImage;
  };
  "generation-vii": {
    icons: SpriteImage;
    "ultra-sun-ultra-moon": SpriteImage;
  };
  "generation-viii": {
    icons: SpriteImage;
  };
}

export interface Sprite extends SpriteImage {
  other: SpriteOther;
  versions: SpriteVersion;
}

export interface Stat {
  base_stat: number;
  effort: number;
  stat: {
    name: string;
    url: string;
  };
}

export interface Type {
  slot: number;
  type: {
    name: string;
    url: string;
  };
}

export interface Pokemon {
  abilities: Ability[];
  base_experience: number;
  forms: Form[];
  game_indices: GameIndex[];
  height: number;
  held_items: HeldItem[];
  id: number;
  is_default: boolean;
  location_area_encounters: string;
  moves: Move[];
  name: string;
  order: number;
  past_types: PastType[];
  species: {
    name: string;
    url: string;
  };
  sprites: Sprite;
  stats: Stat[];
  types: Type[];
  weight: number;
}
