import {
  BaseQueryFn,
  createApi,
  fetchBaseQuery,
  // fetchBaseQuery,
} from "@reduxjs/toolkit/query/react";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { Pokemon } from "../interfaces/pokemon.interface";

const axiosBaseQuery =
  ({
    baseUrl,
  }: {
    baseUrl: string;
  }): BaseQueryFn<
    {
      url: string;
      method: AxiosRequestConfig["method"];
      headers?: AxiosRequestConfig["headers"];
      data?: AxiosRequestConfig["data"];
      params?: AxiosRequestConfig["params"];
    },
    unknown,
    unknown
  > =>
  async ({ url, method, data, params }) => {
    try {
      const result = await axios({
        url: baseUrl + url,
        method,
        headers: { authorization: "Bearer accessToken" },
        data,
        params,
      });
      return { data: result.data };
    } catch (axiosError) {
      const err = axiosError as AxiosError;
      return {
        error: {
          status: err.response?.status,
          data: err.response?.data || err.message,
        },
      };
    }
  };

export const pokemonApi = createApi({
  reducerPath: "pokemonApi",
  tagTypes: ["Pokemon"],
  baseQuery: axiosBaseQuery({
    baseUrl: "https://pokeapi.co/api/v2/",
  }),
  // baseQuery: fetchBaseQuery({
  //   baseUrl: "https://pokeapi.co/api/v2/",
  //   // prepareHeaders(headers) {
  //   //   headers.set("authorization", "Bearer ...");
  //   //   return headers;
  //   // },
  // }),
  endpoints: (build) => ({
    getPokemonList: build.query<any, void>({
      query() {
        return { url: `pokemon`, method: "GET" };
      },
      // providesTags(result) {
      //   if (result) {
      //     const final = [
      //       ...result.map(({ id }: { id: string }) => ({
      //         type: "Pokemon" as const,
      //         id,
      //       })),
      //       { type: "Pokemon" as const, id: "LIST" },
      //     ];
      //     return final;
      //   }
      //   return [{ type: "Pokemon", id: "LIST" }];
      // },
    }),
    getPokemonByName: build.query<Pokemon, string>({
      query(name) {
        return { url: `pokemon/${name}`, method: "GET" };
      },
    }),
    getPokemonById: build.query<Pokemon, number>({
      query(id) {
        return { url: `pokemon/${id}`, method: "GET" };
      },
    }),
  }),
});

export const {
  useGetPokemonListQuery,
  useGetPokemonByNameQuery,
  useGetPokemonByIdQuery,
} = pokemonApi;
