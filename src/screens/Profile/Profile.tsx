import { NativeStackScreenProps } from "@react-navigation/native-stack";
import {
  SafeAreaView,
  StatusBar,
  View,
  Image,
  Text,
  TouchableWithoutFeedback,
  ScrollView,
} from "react-native";
import { RootStackParamList } from "../../../App";
import Headers from "../../components/Header";
import styles from "../../themes/global";
import {
  Grass,
  Poison,
  Fire,
  Flying,
  Ice,
  Psychic,
  Pokeball,
  Circle,
  Dot10x5,
} from "../../images/icons";
import Badge from "../../components/badge/Badge";
import { useState } from "react";
import About from "./about/About";
import Stats from "./stats/Stats";
import Evolution from "./evolution/Evolution";

type ProfileProps = NativeStackScreenProps<RootStackParamList, "Profile">;
const Profile = ({ route, navigation }: ProfileProps) => {
  const [catalog, setCatalog] = useState(0);

  return (
    <SafeAreaView style={[styles.backgroundType_grass, { flex: 1 }]}>
      <StatusBar barStyle={"dark-content"} />
      <Headers color="white" goBack={() => navigation.goBack()} text={""} />
      <View style={{ flex: 1, flexDirection: "column" }}>
        <View
          style={{
            flexDirection: "column",
            paddingHorizontal: 40,
            paddingVertical: 15,
          }}
        >
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
              marginVertical: 50,
              position: "relative",
            }}
          >
            <Image
              style={{
                resizeMode: "contain",
                width: 125,
                height: 125,
                position: "relative",
                zIndex: 10,
              }}
              source={{
                uri: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/1.png",
              }}
            />
            <Circle
              width={125}
              height={125}
              color="#ffffff"
              style={{
                position: "absolute",
                opacity: 0.5,
              }}
            />
            <Dot10x5
              height={140}
              color="#ffffff"
              style={{
                position: "absolute",
                right: "-55%",
                top: "50%",
                opacity: 0.5,
              }}
            />

            <View>
              <Text style={[styles.fontFilterTitle, styles.text_number]}>
                #001
              </Text>
              <Text
                style={[
                  styles.fontApplicationTitle,
                  styles.text_white,
                  { marginBottom: 5 },
                ]}
              >
                Bulbasaur
              </Text>
              <View style={{ flexDirection: "row", gap: 5 }}>
                <Badge
                  icon={<Grass color="#ffffff" width="15" height="15" />}
                  color={styles.bgColor_grass}
                  text="Grass"
                />
                <Badge
                  icon={<Poison color="#ffffff" width="15" height="15" />}
                  color={styles.bgColor_poison}
                  text="Poison"
                />
              </View>
            </View>
          </View>

          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            {["Profile", "Stats", "Evolution"].map((item, index) => (
              <View key={index} style={{ position: "relative" }}>
                {catalog === index ? (
                  <Pokeball
                    width={100}
                    height={100}
                    color="#ffffff"
                    style={{
                      position: "absolute",
                      left: "50%",
                      top: -15,
                      transform: [{ translateX: -50 }],
                      opacity: 0.5,
                    }}
                  />
                ) : null}

                <TouchableWithoutFeedback onPress={() => setCatalog(index)}>
                  <Text
                    style={[
                      catalog === index
                        ? styles.fontFilterTitle
                        : styles.fontDescription,
                      styles.text_white,
                      catalog === index ? { opacity: 1 } : { opacity: 0.5 },
                    ]}
                  >
                    {item}
                  </Text>
                </TouchableWithoutFeedback>
              </View>
            ))}
          </View>
        </View>
        <ScrollView
          style={[
            styles.background_white,
            {
              // paddingVertical: 40,
              // paddingHorizontal: 30,
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
              // flex: 1,
            },
          ]}
        >
          <View style={{ padding: 30, flex: 1 }}>
            {catalog === 0 ? (
              <About />
            ) : catalog === 1 ? (
              <Stats />
            ) : (
              <Evolution />
            )}
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

export default Profile;
