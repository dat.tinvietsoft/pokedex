import React from "react";
import { Text, View } from "react-native";
import * as Progress from "react-native-progress";
import styles from "../../../themes/global";
import {
  Bug,
  Dark,
  Dragon,
  Electric,
  Fairy,
  Fighting,
  Fire,
  Flying,
  Ghost,
  Grass,
  Ground,
  Ice,
  Normal,
  Poison,
  Psychic,
  Rock,
  Steel,
  Water,
} from "../../../images/icons";
import Badge from "../../../components/badge/Badge";

type Props = {};

const Stats = (props: Props) => {
  const baseStats = [
    {
      title: "HP",
      process: 45,
      min: 200,
      max: 294,
    },
    {
      title: "Attack",
      process: 49,
      min: 92,
      max: 216,
    },
    {
      title: "Defense",
      process: 49,
      min: 92,
      max: 216,
    },
    {
      title: "Sp. Atk",
      process: 49,
      min: 92,
      max: 216,
    },
    {
      title: "Sp. Def",
      process: 49,
      min: 92,
      max: 216,
    },
    {
      title: "Speed",
      process: 49,
      min: 92,
      max: 216,
    },
  ];
  const badges = [
    {
      icon: <Normal color="#ffffff" width="15" height="15" />,
      bgColor: styles.bgColor_normal,
    },
    {
      icon: <Fire color="#ffffff" width="15" height="15" />,
      bgColor: styles.bgColor_fire,
      effect: "2",
    },
    {
      icon: <Water color="#ffffff" width="15" height="15" />,
      bgColor: styles.bgColor_water,
      effect: "½",
    },
    {
      icon: <Electric color="#ffffff" width="15" height="15" />,
      bgColor: styles.bgColor_electric,
      effect: "½",
    },
    {
      icon: <Grass color="#ffffff" width="15" height="15" />,
      bgColor: styles.bgColor_grass,
      effect: "¼",
    },
    {
      icon: <Ice color="#ffffff" width="15" height="15" />,
      bgColor: styles.bgColor_ice,
      effect: "2",
    },
    {
      icon: <Fighting color="#ffffff" width="15" height="15" />,
      bgColor: styles.bgColor_fighting,
      effect: "½",
    },
    {
      icon: <Poison color="#ffffff" width="15" height="15" />,
      bgColor: styles.bgColor_poison,
    },
    {
      icon: <Ground color="#ffffff" width="15" height="15" />,
      bgColor: styles.bgColor_ground,
    },
    {
      icon: <Flying color="#ffffff" width="15" height="15" />,
      bgColor: styles.bgColor_flying,
      effect: "2",
    },
    {
      icon: <Psychic color="#ffffff" width="15" height="15" />,
      bgColor: styles.bgColor_psychic,
      effect: "2",
    },
    {
      icon: <Bug color="#ffffff" width="15" height="15" />,
      bgColor: styles.bgColor_bug,
    },
    {
      icon: <Rock color="#ffffff" width="15" height="15" />,
      bgColor: styles.bgColor_rock,
    },
    {
      icon: <Ghost color="#ffffff" width="15" height="15" />,
      bgColor: styles.bgColor_ghost,
    },
    {
      icon: <Dragon color="#ffffff" width="15" height="15" />,
      bgColor: styles.bgColor_dragon,
    },
    {
      icon: <Dark color="#ffffff" width="15" height="15" />,
      bgColor: styles.bgColor_dark,
    },
    {
      icon: <Steel color="#ffffff" width="15" height="15" />,
      bgColor: styles.bgColor_steel,
    },
    {
      icon: <Fairy color="#ffffff" width="15" height="15" />,
      bgColor: styles.bgColor_fairy,
      effect: "½",
    },
  ];

  return (
    <>
      <View>
        <Text
          style={[
            styles.fontFilterTitle,
            styles.colorType_grass,
            { marginVertical: 10 },
          ]}
        >
          Base Stats
        </Text>

        {baseStats.map((item, index) => (
          <View
            key={index}
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              gap: 10,
              marginVertical: 10,
            }}
          >
            <View style={{ flex: 1 }}>
              <Text style={[styles.fontPokemonType, styles.text_black]}>
                {item.title}
              </Text>
            </View>

            <View
              style={{
                flex: 4,
                display: "flex",
                flexDirection: "row",
                gap: 10,
                justifyContent: "space-between",
              }}
            >
              <View
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                  gap: 10,
                }}
              >
                <Text style={[styles.fontDescription, styles.text_gray]}>
                  {item.process}
                </Text>
                <Progress.Bar
                  width={100}
                  progress={item.process / item.max}
                  borderWidth={0}
                  color={styles.colorType_grass.color}
                />
                <View
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "flex-end",
                    gap: 10,
                  }}
                >
                  <Text
                    style={[
                      styles.fontDescription,
                      styles.text_gray,
                      { textAlign: "right" },
                    ]}
                  >
                    {item.min}
                  </Text>
                  <Text
                    style={[
                      styles.fontDescription,
                      styles.text_gray,
                      { textAlign: "right" },
                    ]}
                  >
                    {item.max}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        ))}
        <View
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            gap: 10,
            marginVertical: 10,
          }}
        >
          <View style={{ flex: 1 }}>
            <Text style={[styles.fontPokemonType, styles.text_black]}>
              Total
            </Text>
          </View>

          <View
            style={{
              flex: 3,
              display: "flex",
              flexDirection: "row",
              flexWrap: "wrap",
            }}
          >
            <View
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                flexWrap: "wrap",
                gap: 10,
              }}
            >
              <Text style={[styles.fontFilterTitle, styles.text_gray]}>
                318
              </Text>
              <Text style={[styles.fontPokemonType, styles.text_black]}>
                Min
              </Text>
              <Text style={[styles.fontPokemonType, styles.text_black]}>
                Max
              </Text>
            </View>
          </View>
        </View>

        <Text style={[styles.fontPokemonType, styles.text_gray]}>
          The ranges shown on the right are for a level 100 Pokémon. Maximum
          values are based on a beneficial nature, 252 EVs, 31 IVs; minimum
          values are based on a hindering nature, 0 EVs, 0 IVs.
        </Text>
      </View>

      <View>
        <Text
          style={[
            styles.fontFilterTitle,
            styles.colorType_grass,
            { marginVertical: 10 },
          ]}
        >
          Type Defenses
        </Text>

        <Text style={[styles.fontDescription, styles.text_gray]}>
          The effectiveness of each type on Bulbasaur.
        </Text>
        <View
          style={{
            display: "flex",
            flexDirection: "row",
            flexWrap: "wrap",
            gap: 10,
          }}
        >
          {badges.map((badge, index) => (
            <Badge key={index} icon={badge.icon} color={badge.bgColor} />
          ))}
        </View>
      </View>
    </>
  );
};

export default Stats;
