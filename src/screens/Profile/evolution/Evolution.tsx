import React from "react";
import { Image, Text, View } from "react-native";
import styles from "../../../themes/global";
import { Pokeball, RightArrow } from "../../../images/icons";

type Props = {};

const Evolution = (props: Props) => {
  return (
    <>
      <View>
        <Text
          style={[
            styles.fontFilterTitle,
            styles.colorType_grass,
            { marginVertical: 10 },
          ]}
        >
          Evolution Chart
        </Text>

        <View
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            marginVertical: 30,
            alignItems: "center",
          }}
        >
          <View
            style={{
              display: "flex",
              alignItems: "center",
            }}
          >
            <View
              style={{
                position: "relative",
              }}
            >
              <Pokeball
                width={100}
                height={100}
                color="#f5f5f5"
                style={{
                  position: "absolute",
                  top: "50%",
                  left: "50%",
                  transform: [{ translateX: -50 }, { translateY: -50 }],
                }}
              />
              <Image
                style={{
                  resizeMode: "contain",
                  width: 75,
                  height: 75,
                }}
                source={{
                  uri: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/1.png",
                }}
              />
            </View>
            <Text
              style={[
                styles.fontPokemonType,
                styles.text_gray,
                { marginTop: 10 },
              ]}
            >
              #001
            </Text>
            <Text style={[styles.fontFilterTitle, styles.text_black]}>
              Bulbasaur
            </Text>
          </View>

          <View style={{ display: "flex", alignItems: "center", gap: 10 }}>
            <RightArrow width={25} height={25} color="#f5f5f5" />
            <Text style={[styles.fontPokemonNumber, styles.text_black]}>
              Level 16
            </Text>
          </View>

          <View
            style={{
              display: "flex",
              alignItems: "center",
            }}
          >
            <View
              style={{
                position: "relative",
              }}
            >
              <Pokeball
                width={100}
                height={100}
                color="#f5f5f5"
                style={{
                  position: "absolute",
                  top: "50%",
                  left: "50%",
                  transform: [{ translateX: -50 }, { translateY: -50 }],
                }}
              />
              <Image
                style={{
                  resizeMode: "contain",
                  width: 75,
                  height: 75,
                }}
                source={{
                  uri: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/2.png",
                }}
              />
            </View>
            <Text
              style={[
                styles.fontPokemonType,
                styles.text_gray,
                { marginTop: 10 },
              ]}
            >
              #002
            </Text>
            <Text style={[styles.fontFilterTitle, styles.text_black]}>
              Ivysaur
            </Text>
          </View>
        </View>

        <View
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            marginVertical: 30,
            alignItems: "center",
          }}
        >
          <View
            style={{
              display: "flex",
              alignItems: "center",
            }}
          >
            <View
              style={{
                position: "relative",
              }}
            >
              <Pokeball
                width={100}
                height={100}
                color="#f5f5f5"
                style={{
                  position: "absolute",
                  top: "50%",
                  left: "50%",
                  transform: [{ translateX: -50 }, { translateY: -50 }],
                }}
              />
              <Image
                style={{
                  resizeMode: "contain",
                  width: 75,
                  height: 75,
                }}
                source={{
                  uri: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/2.png",
                }}
              />
            </View>
            <Text
              style={[
                styles.fontPokemonType,
                styles.text_gray,
                { marginTop: 10 },
              ]}
            >
              #002
            </Text>
            <Text style={[styles.fontFilterTitle, styles.text_black]}>
              Ivysaur
            </Text>
          </View>

          <View style={{ display: "flex", alignItems: "center", gap: 10 }}>
            <RightArrow width={25} height={25} color="#f5f5f5" />
            <Text style={[styles.fontPokemonNumber, styles.text_black]}>
              Level 16
            </Text>
          </View>

          <View
            style={{
              display: "flex",
              alignItems: "center",
            }}
          >
            <View
              style={{
                position: "relative",
              }}
            >
              <Pokeball
                width={100}
                height={100}
                color="#f5f5f5"
                style={{
                  position: "absolute",
                  top: "50%",
                  left: "50%",
                  transform: [{ translateX: -50 }, { translateY: -50 }],
                }}
              />
              <Image
                style={{
                  resizeMode: "contain",
                  width: 75,
                  height: 75,
                }}
                source={{
                  uri: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/3.png",
                }}
              />
            </View>
            <Text
              style={[
                styles.fontPokemonType,
                styles.text_gray,
                { marginTop: 10 },
              ]}
            >
              #003
            </Text>
            <Text style={[styles.fontFilterTitle, styles.text_black]}>
              Venusaur
            </Text>
          </View>
        </View>
      </View>
    </>
  );
};

export default Evolution;
