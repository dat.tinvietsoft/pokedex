import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { SafeAreaView, StatusBar, View, Image, Text } from "react-native";
import { RootStackParamList } from "../../../../App";
import Headers from "../../../components/Header";
import styles from "../../../themes/global";
import {
  Grass,
  Poison,
  Fire,
  Flying,
  Ice,
  Psychic,
  Pokeball,
  Circle,
  Dot10x5,
} from "../../../images/icons";
import Badge from "../../../components/badge/Badge";
import {
  useGetPokemonByNameQuery,
  useGetPokemonListQuery,
} from "../../../services/pokemonAPI";

const About = () => {
  const { data, error, isLoading } = useGetPokemonListQuery();
  console.log(data.results);
  const pokedexData = [
    {
      title: "Species",
      info: "Seed Pokémon",
    },
    {
      title: "Height",
      info: "0.7m",
      subInfo: "(2′04″)",
    },
    {
      title: "Weight",
      info: "6.9kg",
      subInfo: "(15.2 lbs)",
    },
    {
      title: "Abilities",
      info: "1. Overgrow",
      subInfo: "Chlorophyll (hidden ability)",
    },
    {
      title: "Weaknesses",
      badges: [
        {
          icon: <Fire color="#ffffff" width="15" height="15" />,
          bgColor: styles.bgColor_fire,
        },
        {
          icon: <Flying color="#ffffff" width="15" height="15" />,
          bgColor: styles.bgColor_flying,
        },
        {
          icon: <Ice color="#ffffff" width="15" height="15" />,
          bgColor: styles.bgColor_ice,
        },
        {
          icon: <Psychic color="#ffffff" width="15" height="15" />,
          bgColor: styles.bgColor_psychic,
        },
      ],
    },
  ];

  const training = [
    {
      title: "EV Yield",
      info: "1 Special Attack",
    },
    {
      title: "Catch Rate",
      info: "45",
      subInfo: "(5.9% with PokéBall, full HP)",
    },
    {
      title: "Base Friendship",
      info: "70",
      subInfo: "(normal)",
    },
    {
      title: "Base Exp",
      info: "64",
    },
    {
      title: "Growth Rate",
      info: "Medium Slow",
    },
  ];

  const breeding = [
    {
      title: "Gender",
      info: "♀ 87.5%, ♂ 12.5%",
    },
    {
      title: "Egg Groups",
      info: "Grass, Monster",
    },
    {
      title: "Egg Cycles",
      info: "20",
      subInfo: "(4,884 - 5,140 steps)",
    },
  ];

  const location = [
    {
      title: "001",
      info: "(Red/Blue/Yellow)",
      subInfo: "",
    },
    {
      title: "226",
      info: "(Gold/Silver/Crystal)",
    },
    {
      title: "001",
      info: "(FireRed/LeafGreen)",
    },
    {
      title: "231",
      info: "(HeartGold/SoulSilver)",
    },
    {
      title: "080",
      info: "(X/Y - Central Kalos)",
    },
    {
      title: "001",
      info: "(Let's Go Pikachu/Let's Go Eevee)",
    },
  ];

  return (
    <>
      <View>
        <Text style={[styles.fontDescription, styles.text_gray]}>
          Bulbasaur can be seen napping in bright sunlight. There is a seed on
          its back. By soaking up the sun's rays, the seed grows progressively
          larger.
        </Text>
      </View>

      <View>
        <Text
          style={[
            styles.fontFilterTitle,
            styles.colorType_grass,
            { marginVertical: 10 },
          ]}
        >
          Pokédex Data
        </Text>

        {pokedexData.map((item, index) => (
          <View
            key={index}
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              gap: 10,
              marginVertical: 10,
            }}
          >
            <View style={{ flex: 1 }}>
              <Text style={[styles.fontPokemonType, styles.text_black]}>
                {item.title}
              </Text>
            </View>

            <View
              style={{
                flex: 3,
                display: "flex",
                flexDirection: "row",
                flexWrap: "wrap",
                gap: 10,
              }}
            >
              <View
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  flexWrap: "wrap",
                }}
              >
                {item.info ? (
                  <Text style={[styles.fontDescription, styles.text_gray]}>
                    {item.info}&nbsp;
                  </Text>
                ) : null}
                {item.subInfo ? (
                  <Text style={[styles.fontPokemonType, styles.text_gray]}>
                    {item.subInfo}
                  </Text>
                ) : null}

                <View
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    flexWrap: "wrap",
                    gap: 10,
                  }}
                >
                  {item.badges?.map((badge, index) => (
                    <Badge
                      key={index}
                      icon={badge.icon}
                      color={badge.bgColor}
                    />
                  ))}
                </View>
              </View>
            </View>
          </View>
        ))}
      </View>

      <View>
        <Text
          style={[
            styles.fontFilterTitle,
            styles.colorType_grass,
            { marginVertical: 10 },
          ]}
        >
          Training
        </Text>

        {training.map((item, index) => (
          <View
            key={index}
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              gap: 10,
              marginVertical: 10,
            }}
          >
            <View style={{ flex: 1 }}>
              <Text style={[styles.fontPokemonType, styles.text_black]}>
                {item.title}
              </Text>
            </View>

            <View
              style={{
                flex: 3,
                display: "flex",
                flexDirection: "row",
                gap: 10,
              }}
            >
              <View
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  flexWrap: "wrap",
                }}
              >
                {item.info ? (
                  <Text style={[styles.fontDescription, styles.text_gray]}>
                    {item.info}&nbsp;
                  </Text>
                ) : null}
                {item.subInfo ? (
                  <Text style={[styles.fontPokemonType, styles.text_gray]}>
                    {item.subInfo}
                  </Text>
                ) : null}
              </View>
            </View>
          </View>
        ))}
      </View>

      <View>
        <Text
          style={[
            styles.fontFilterTitle,
            styles.colorType_grass,
            { marginVertical: 10 },
          ]}
        >
          Breeding
        </Text>

        {breeding.map((item, index) => (
          <View
            key={index}
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              gap: 10,
              marginVertical: 10,
            }}
          >
            <View style={{ flex: 1 }}>
              <Text style={[styles.fontPokemonType, styles.text_black]}>
                {item.title}
              </Text>
            </View>

            <View
              style={{
                flex: 3,
                display: "flex",
                flexDirection: "row",
                gap: 10,
              }}
            >
              <View
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  flexWrap: "wrap",
                }}
              >
                {item.info ? (
                  <Text style={[styles.fontDescription, styles.text_gray]}>
                    {item.info}&nbsp;
                  </Text>
                ) : null}
                {item.subInfo ? (
                  <Text style={[styles.fontPokemonType, styles.text_gray]}>
                    {item.subInfo}
                  </Text>
                ) : null}
              </View>
            </View>
          </View>
        ))}
      </View>

      <View>
        <Text
          style={[
            styles.fontFilterTitle,
            styles.colorType_grass,
            { marginVertical: 10 },
          ]}
        >
          Location
        </Text>

        {location.map((item, index) => (
          <View
            key={index}
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              gap: 10,
              marginVertical: 10,
            }}
          >
            <View style={{ flex: 1 }}>
              <Text style={[styles.fontPokemonType, styles.text_black]}>
                {item.title}
              </Text>
            </View>

            <View
              style={{
                flex: 3,
                display: "flex",
                flexDirection: "row",
                gap: 10,
              }}
            >
              <View
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  flexWrap: "wrap",
                }}
              >
                {item.info ? (
                  <Text style={[styles.fontDescription, styles.text_gray]}>
                    {item.info}&nbsp;
                  </Text>
                ) : null}
                {item.subInfo ? (
                  <Text style={[styles.fontPokemonType, styles.text_gray]}>
                    {item.subInfo}
                  </Text>
                ) : null}
              </View>
            </View>
          </View>
        ))}
      </View>
    </>
  );
};

export default About;
