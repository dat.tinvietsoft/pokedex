import { NativeStackScreenProps } from "@react-navigation/native-stack";
import React, { useEffect, useContext, useState, useRef } from "react";
import {
  SafeAreaView,
  StatusBar,
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  FlatList,
  ActivityIndicator,
  ScrollView,
  TouchableWithoutFeedback,
} from "react-native";
import { RootStackParamList } from "../../../../App";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { useDispatch, useSelector } from "react-redux";
import RangeSlider, { Slider } from "react-native-range-slider-expo";
import { BlurView } from "expo-blur";
import styles from "../../../themes/global";
// Icon
import Modal from "react-native-modal";

import { useGetPokemonByNameQuery } from "../../../services/pokemonAPI";
type Props = {
  modalVisible: boolean;
  dataItem: Array<item>;
  callBack: any;
};
type item = {
  id: number;
  text: string;
  status: number;
  bgColor: string;
};
const ModalSort = (props: Props) => {
  console.log("modalVisible", props.modalVisible);
  const [dataItem, setDataItem] = useState(props.dataItem);
  const [modalVisible, setModalVisible] = useState<boolean>(props.modalVisible);
  useEffect(() => {
    console.log("effect");
    console.log(props.dataItem);
    setModalVisible(props.modalVisible);
  }, [props.modalVisible]);

  const renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => {
          choosenType(item.id, item.status);
        }}
        style={[
          item.status == 1
            ? {
                shadowColor: "#000",
                shadowOffset: {
                  width: 0,
                  height: 1,
                },
                shadowOpacity: 0.3,
                shadowRadius: 2,

                elevation: 5,
              }
            : null,
          {
            backgroundColor:
              item.status == 0
                ? styles.background_pressedInput.backgroundColor
                : item.bgColor,
            padding: 20,
            borderRadius: 10,
            marginVertical: 5,
            justifyContent: "center",
            alignItems: "center",
          },
        ]}
      >
        <Text style={item.status == 0 ? styles.text_gray : styles.text_white}>
          {" "}
          {item.text}
        </Text>
      </TouchableOpacity>
    );
  };
  const choosenType = (id: number, status: number) => {
    let arr = [...props.dataItem];
    arr.forEach(function (item) {
      if (item.id == id) {
        item.status = status == 0 ? 1 : 0;
      } else {
        item.status = 0;
      }
    });
    setDataItem(arr);
  };
  const { data } = useGetPokemonByNameQuery("bulbasaur");

  const fetchData = () => {
    console.log(data.sprites.other["official-artwork"].front_default);
  };

  return (
    <>
      <Modal
        swipeDirection="down"
        onSwipeComplete={() => {
          props.callBack(!modalVisible);
          setModalVisible(!modalVisible);
          console.log("swipe");
        }}
        backdropOpacity={0.5}
        // customBackdrop={
        //   <TouchableWithoutFeedback
        //     onPress={() => {
        //       props.callBack(!modalVisible);
        //       setModalVisible(!modalVisible);
        //     }}
        //   >
        //     <View style={{ flex: 1 }} />
        //   </TouchableWithoutFeedback>
        // }
        isVisible={modalVisible}
        style={[
          {
            justifyContent: "flex-end",
            margin: 0,
          },
        ]}
      >
        <View
          style={{
            alignItems: "center",
            justifyContent: "flex-end",
            flex: 1,
          }}
        >
          <View
            style={{
              backgroundColor: "white",
              borderRadius: 8,
              height: 5,
              width: 60,
              marginBottom: 10,
            }}
          ></View>
        </View>
        <View
          style={{
            backgroundColor: "white",
            padding: 30,
            borderRadius: 4,
            borderTopLeftRadius: 30,
            borderTopRightRadius: 30,
          }}
        >
          <Text style={{ fontSize: 25, fontWeight: "bold" }}>Sort</Text>
          <Text style={{ marginBottom: 20 }}>
            Sort Pokémons alphabetically or by National Pokédex number!
          </Text>
          <FlatList
            data={dataItem}
            renderItem={renderItem}
            keyExtractor={(item, index) => String("item" + index)}
          />
        </View>
      </Modal>
    </>
  );
};

export default ModalSort;
