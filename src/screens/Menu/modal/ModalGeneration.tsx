import { NativeStackScreenProps } from "@react-navigation/native-stack";
import React, { useEffect, useContext, useState, useRef } from "react";
import {
  SafeAreaView,
  StatusBar,
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  FlatList,
  ActivityIndicator,
  ScrollView,
} from "react-native";
import { RootStackParamList } from "../../../../App";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { useDispatch, useSelector } from "react-redux";
import RangeSlider, { Slider } from "react-native-range-slider-expo";
import { BlurView } from "expo-blur";
import styles from "../../../themes/global";
// Icon
import Modal from "react-native-modal";
import GestureRecognizer from "react-native-swipe-gestures";
import swipeDirections from "react-native-swipe-gestures";

import { useGetPokemonByNameQuery } from "../../../services/pokemonAPI";
type Props = {
  modalVisible: boolean;
  dataItem: Array<item>;
  callBack: any;
};
type item = {
  id: number;
  text: string;
  status: number;
  bgColor: string;
};
const ModalGeneration = (props: Props) => {
  console.log("modalVisible", props.modalVisible);
  const [dataItem, setDataItem] = useState(props.dataItem);
  const [modalVisible, setModalVisible] = useState<boolean>(props.modalVisible);
  useEffect(() => {
    console.log("effect");
    console.log(props.dataItem);
    setModalVisible(props.modalVisible);
  }, [props.modalVisible]);

  const renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => {
          choosenType(item.id, item.status);
        }}
        style={[
          item.status == 1
            ? {
                shadowColor: "#000",
                shadowOffset: {
                  width: 0,
                  height: 1,
                },
                shadowOpacity: 0.3,
                shadowRadius: 2,

                elevation: 5,
              }
            : null,
          {
            flex: 1,
            marginHorizontal: 5,
            backgroundColor:
              item.status == 0
                ? styles.background_pressedInput.backgroundColor
                : item.bgColor,
            // padding: 20,
            borderRadius: 10,
            marginVertical: 5,
            justifyContent: "center",
            alignItems: "center",
            paddingVertical: 20,
            paddingHorizontal: 10,
          },
        ]}
      >
        <View style={{ flexDirection: "row", marginBottom: 10 }}>
          <View style={{ flex: 1 }}>
            <Image
              style={{
                // position: "absolute",
                resizeMode: "contain",
                width: 50,
                height: 50,
              }}
              source={{
                uri: item.img1,
              }}
            />
          </View>
          <View style={{ flex: 1 }}>
            <Image
              style={{
                // position: "absolute",
                resizeMode: "contain",
                width: 50,
                height: 50,
              }}
              source={{
                uri: item.img2,
              }}
            />
          </View>
          <View style={{ flex: 1 }}>
            <Image
              style={{
                // position: "absolute",
                resizeMode: "contain",
                width: 50,
                height: 50,
              }}
              source={{
                uri: item.img3,
              }}
            />
          </View>
        </View>
        <Text style={item.status == 0 ? styles.text_gray : styles.text_white}>
          {item.text}
        </Text>
      </TouchableOpacity>
    );
  };
  const choosenType = (id: number, status: number) => {
    let arr = [...props.dataItem];
    arr.forEach(function (item) {
      if (item.id == id) {
        item.status = status == 0 ? 1 : 0;
      } else {
        item.status = 0;
      }
    });
    setDataItem(arr);
  };
  const { data } = useGetPokemonByNameQuery("bulbasaur");

  const fetchData = () => {
    console.log(data.sprites.other["official-artwork"].front_default);
  };
  const onSwipe = (gestureName, gestureState) => {
    // const { SWIPE_DOWN } = swipeDirections;
    console.log("onSwipe ", gestureName);
    // switch (gestureName) {
    //   case SWIPE_DOWN:
    //     console.log("down");
    //     break;
    // }
  };
  return (
    <>
      <Modal
        swipeDirection="down"
        onSwipeComplete={() => {
          props.callBack(!modalVisible);
          setModalVisible(!modalVisible);
          console.log("swipe");
        }}
        isVisible={modalVisible}
        style={[
          {
            justifyContent: "flex-end",
            margin: 0,
          },
        ]}
      >
        <View
          style={{
            alignItems: "center",
            justifyContent: "flex-end",
            flex: 1,
          }}
        >
          <View
            style={{
              backgroundColor: "white",
              borderRadius: 8,
              height: 5,
              width: 60,
              marginBottom: 10,
            }}
          ></View>
        </View>
        <View
          style={{
            backgroundColor: "white",
            padding: 30,
            borderRadius: 4,
            borderTopLeftRadius: 30,
            borderTopRightRadius: 30,
            // maxHeight: "80%",
          }}
        >
          <Text style={{ fontSize: 25, fontWeight: "bold" }}>Generations</Text>
          <Text style={[styles.text_gray, { marginBottom: 20 }]}>
            Use search for generations to explore your Pokémon!
          </Text>
          <FlatList
            numColumns={2}
            data={dataItem}
            renderItem={renderItem}
            keyExtractor={(item, index) => String("item" + index)}
          />
        </View>
      </Modal>
    </>
  );
};

export default ModalGeneration;
