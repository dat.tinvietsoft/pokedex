import { NativeStackScreenProps } from "@react-navigation/native-stack";
import React, { useEffect, useContext, useState, useRef } from "react";
import {
  SafeAreaView,
  StatusBar,
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  FlatList,
  ActivityIndicator,
  ScrollView,
} from "react-native";
import { RootStackParamList } from "../../../../App";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { useDispatch, useSelector } from "react-redux";
import RangeSlider, { Slider } from "react-native-range-slider-expo";
import { BlurView } from "expo-blur";
import styles from "../../../themes/global";
// Icon
import Modal from "react-native-modal";
import Bug from "../../../images/icons/types/Bug";
import Dark from "../../../images/icons/types/Dark";
import Dragon from "../../../images/icons/types/Dragon";
import Electric from "../../../images/icons/types/Electric";
import Fairy from "../../../images/icons/types/Fairy";
import Fighting from "../../../images/icons/types/Fighting";
import Fire from "../../../images/icons/types/Fire";
import Flying from "../../../images/icons/types/Flying";
import Ghost from "../../../images/icons/types/Ghost";
import Grass from "../../../images/icons/types/Grass";
import Ground from "../../../images/icons/types/Ground";
import Ice from "../../../images/icons/types/Ice";
import Normal from "../../../images/icons/types/Normal";
import Poison from "../../../images/icons/types/Poison";
import Psychic from "../../../images/icons/types/Psychic";
import Rock from "../../../images/icons/types/Rock";
import Steel from "../../../images/icons/types/Steel";
import Water from "../../../images/icons/types/Water";
// Height
import Medium from "../../../images/icons/heights/Medium";
import Short from "../../../images/icons/heights/Short";
import Tall from "../../../images/icons/heights/Tall";
// Weight
import Heavy from "../../../images/icons/weights/Heavy";
import Light from "../../../images/icons/weights/Light";
import NormalW from "../../../images/icons/weights/Normal";
//
type Props = {
  modalVisible: boolean;
  dataIcon: Array<itemIcon>;
  dataIconWeaknesses: Array<itemIconWeaknesses>;
  dataIconW: Array<itemIconW>;
  dataIconH: Array<itemIconH>;
  callBack: any;
};
type itemIcon = {
  name: string;
  status: number;
  bgColor: string;
};
type itemIconWeaknesses = {
  name: string;
  status: number;
  bgColor: string;
};
type itemIconW = {
  name: string;
  status: number;
  bgColor: string;
};
type itemIconH = {
  name: string;
  status: number;
  bgColor: string;
};
const ModalFilter = (props: Props) => {
  console.log("modalVisible", props.modalVisible);
  const [dataIcon, setDataIcon] = useState(props.dataIcon);
  const [dataIconWeaknesses, setDataIconWeaknesses] = useState(
    props.dataIconWeaknesses
  );
  const [dataIconW, setDataIconW] = useState(props.dataIconW);
  const [dataIconH, setDataIconH] = useState(props.dataIconH);
  const [modalVisible, setModalVisible] = useState<boolean>(props.modalVisible);
  useEffect(() => {
    console.log("effect");
    setModalVisible(props.modalVisible);
  }, [props.modalVisible]);
  const renderItemIcon = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => {
          choosenType(item.name, item.status);
        }}
        style={{
          backgroundColor: item.status == 0 ? "#FFFFFF" : item.bgColor,
          padding: 10,
          borderRadius: 50,
          margin: 10,
        }}
      >
        {renderIcon(item.name, item.status, item.bgColor)}
      </TouchableOpacity>
    );
  };
  const choosenType = (name: string, status: number) => {
    let arr = [...props.dataIcon];
    arr.forEach(function (item) {
      if (item.name == name) {
        item.status = status == 0 ? 1 : 0;
      }
    });
    setDataIcon(arr);
  };

  const renderItemIconWeaknesses = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => {
          choosenTypeWeaknesses(item.name, item.status);
        }}
        style={{
          backgroundColor: item.status == 0 ? "#FFFFFF" : item.bgColor,
          padding: 10,
          borderRadius: 50,
          margin: 10,
        }}
      >
        {renderIcon(item.name, item.status, item.bgColor)}
      </TouchableOpacity>
    );
  };

  const choosenTypeWeaknesses = (name: string, status: number) => {
    let arr = [...dataIconWeaknesses];
    arr.forEach(function (item) {
      if (item.name == name) {
        item.status = status == 0 ? 1 : 0;
      }
    });
    setDataIconWeaknesses(arr);
  };
  const renderItemIconH = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => {
          choosenTypeH(item.name, item.status);
        }}
        style={{
          backgroundColor: item.status == 0 ? "#FFFFFF" : item.bgColor,
          padding: 10,
          borderRadius: 50,
          margin: 10,
        }}
      >
        {renderIcon(item.name, item.status, item.bgColor)}
      </TouchableOpacity>
    );
  };
  const choosenTypeH = (name: string, status: number) => {
    let arr = [...dataIconH];
    arr.forEach(function (item) {
      if (item.name == name) {
        item.status = status == 0 ? 1 : 0;
      }
    });
    setDataIconH(arr);
  };
  const renderItemIconW = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => {
          choosenTypeW(item.name, item.status);
        }}
        style={{
          backgroundColor: item.status == 0 ? "#FFFFFF" : item.bgColor,
          padding: 10,
          borderRadius: 50,
          margin: 10,
        }}
      >
        {renderIcon(item.name, item.status, item.bgColor)}
      </TouchableOpacity>
    );
  };
  const choosenTypeW = (name: string, status: number) => {
    let arr = [...dataIconW];
    arr.forEach(function (item) {
      if (item.name == name) {
        item.status = status == 0 ? 1 : 0;
      }
    });
    setDataIconW(arr);
  };
  const renderIcon = (name: string, status: number, color: string) => {
    switch (name) {
      case "bug":
        return <Bug color={status == 0 ? color : "#FFFFFF"} />;
      case "dark":
        return <Dark color={status == 0 ? color : "#FFFFFF"} />;
      case "dragon":
        return <Dragon color={status == 0 ? color : "#FFFFFF"} />;
      case "electric":
        return <Electric color={status == 0 ? color : "#FFFFFF"} />;
      case "fairy":
        return <Fairy color={status == 0 ? color : "#FFFFFF"} />;
      case "fighting":
        return <Fighting color={status == 0 ? color : "#FFFFFF"} />;
      case "fire":
        return <Fire color={status == 0 ? color : "#FFFFFF"} />;
      case "flying":
        return <Flying color={status == 0 ? color : "#FFFFFF"} />;
      case "ghost":
        return <Ghost color={status == 0 ? color : "#FFFFFF"} />;
      case "grass":
        return <Grass color={status == 0 ? color : "#FFFFFF"} />;
      case "ground":
        return <Ground color={status == 0 ? color : "#FFFFFF"} />;
      case "ice":
        return <Ice color={status == 0 ? color : "#FFFFFF"} />;
      case "normal":
        return <Normal color={status == 0 ? color : "#FFFFFF"} />;
      case "poison":
        return <Poison color={status == 0 ? color : "#FFFFFF"} />;
      case "psychic":
        return <Psychic color={status == 0 ? color : "#FFFFFF"} />;
      case "rock":
        return <Rock color={status == 0 ? color : "#FFFFFF"} />;
      case "steel":
        return <Steel color={status == 0 ? color : "#FFFFFF"} />;
      case "water":
        return <Water color={status == 0 ? color : "#FFFFFF"} />;
      case "medium":
        return <Medium color={status == 0 ? color : "#FFFFFF"} />;
      case "short":
        return <Short color={status == 0 ? color : "#FFFFFF"} />;
      case "tall":
        return <Tall color={status == 0 ? color : "#FFFFFF"} />;
      case "heavy":
        return <Heavy color={status == 0 ? color : "#FFFFFF"} />;
      case "light":
        return <Light color={status == 0 ? color : "#FFFFFF"} />;
      case "normalw":
        return <NormalW color={status == 0 ? color : "#FFFFFF"} />;
      default:
        console.log("out case");
    }
  };
  return (
    <>
      <Modal
        swipeDirection="down"
        onSwipeComplete={() => {
          props.callBack(!modalVisible);
          setModalVisible(!modalVisible);
          console.log("swipe");
        }}
        isVisible={modalVisible}
        style={[
          {
            justifyContent: "flex-end",
            margin: 0,
          },
        ]}
      >
        <View
          style={{
            alignItems: "center",
            justifyContent: "flex-end",
            flex: 1,
          }}
        >
          <View
            style={{
              backgroundColor: "white",
              borderRadius: 8,
              height: 5,
              width: 60,
              marginBottom: 10,
            }}
          ></View>
        </View>
        <View
          style={{
            backgroundColor: "white",
            padding: 30,
            borderRadius: 4,
            borderTopLeftRadius: 30,
            borderTopRightRadius: 30,
          }}
        >
          <Text style={{ fontSize: 25, fontWeight: "bold" }}>Filters</Text>
          <Text>
            Use advanced search to explore Pokémon by type, weakness, height and
            more!
          </Text>
          <View style={{ marginTop: 20 }}>
            <Text style={{ fontSize: 15, fontWeight: "bold" }}>Types</Text>
            <View>
              <FlatList
                data={props.dataIcon}
                horizontal={true}
                renderItem={renderItemIcon}
                keyExtractor={(item, index) => String("ic" + index)}
              />
            </View>
          </View>
          <View style={{ marginTop: 20 }}>
            <Text style={{ fontSize: 15, fontWeight: "bold" }}>Weaknesses</Text>
            <View>
              <FlatList
                data={dataIconWeaknesses}
                horizontal={true}
                renderItem={renderItemIconWeaknesses}
                keyExtractor={(item, index) => String("ic2" + index)}
              />
            </View>
          </View>
          <View style={{ marginTop: 20 }}>
            <Text style={{ fontSize: 15, fontWeight: "bold" }}>Height</Text>
            <View>
              <FlatList
                data={dataIconH}
                horizontal={true}
                renderItem={renderItemIconH}
                keyExtractor={(item, index) => String("icH" + index)}
              />
            </View>
          </View>
          <View style={{ marginTop: 20 }}>
            <Text style={{ fontSize: 15, fontWeight: "bold" }}>Weight</Text>
            <View>
              <FlatList
                data={dataIconW}
                horizontal={true}
                renderItem={renderItemIconW}
                keyExtractor={(item, index) => String("icW" + index)}
              />
            </View>
          </View>
          <View style={{ marginTop: 20, marginBottom: 40 }}>
            <Text style={{ fontSize: 15, fontWeight: "bold" }}>
              Number Range
            </Text>
            {/* <RangeSlider
                    min={0}
                    max={25}
                    fromValueOnChange={(value) => setFromValue(value)}
                    toValueOnChange={(value) => setToValue(value)}
                    initialFromValue={fromValue}
                    initialToValue={toValue}
                    fromKnobColor="#FF6568"
                    toKnobColor="#FF6568"
                    showRangeLabels={false}
                    valueLabelsBackgroundColor="black"
                    inRangeBarColor="#FF6568"
                    styleSize={"small"}
                    knobSize={24}
                    barHeight={8}
                  /> */}
            <View style={{ paddingVertical: 10 }}></View>
          </View>
          {/* <View style={{ flexDirection: "row" }}>
                  <View style={{ alignItems: "flex-start", flex: 1 }}>
                    <Text>{fromValue}</Text>
                  </View>
                  <View style={{ alignItems: "flex-end", flex: 1 }}>
                    <Text>{toValue}</Text>
                  </View>
                </View> */}
          <View style={{ flexDirection: "row", marginTop: 20 }}>
            <View style={{ flex: 1, paddingRight: 10 }}>
              <TouchableOpacity
                style={[
                  styles.buttonBottomModal,
                  styles.background_pressedInput,
                ]}
                onPress={() => {
                  props.callBack(!modalVisible);
                  setModalVisible(!modalVisible);
                }}
              >
                <Text style={styles.text_gray}>Reset</Text>
              </TouchableOpacity>
            </View>
            <View style={{ flex: 1, paddingLeft: 10 }}>
              <TouchableOpacity
                style={[
                  styles.buttonBottomModal,
                  styles.backgroundType_psychic,
                  {
                    shadowColor: "#000",
                    shadowOffset: {
                      width: 0,
                      height: 5,
                    },
                    shadowOpacity: 0.34,
                    shadowRadius: 6.27,

                    elevation: 10,
                  },
                ]}
                onPress={() => {
                  props.callBack(!modalVisible);
                  setModalVisible(!modalVisible);
                }}
              >
                <Text style={styles.text_white}>Apply</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </>
  );
};

export default ModalFilter;
