import { NativeStackScreenProps } from "@react-navigation/native-stack";
import React, { useEffect, useContext, useState, useRef } from "react";
import {
  SafeAreaView,
  StatusBar,
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  FlatList,
  ActivityIndicator,
  ScrollView,
  Modal,
} from "react-native";
import { RootStackParamList } from "../../../App";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { useDispatch, useSelector } from "react-redux";
import styles from "../../themes/global";
// Top Right
import Generation from "../../images/icons/Generation";
import Sort from "../../images/icons/Sort";
import Filter from "../../images/icons/Filter";
//
import ModalFilter from "./modal/ModalFilter";
import ModalSort from "./modal/ModalSort";
import ModalGeneration from "./modal/ModalGeneration";
//
import axios from "axios";
//
import { Grass } from "../../images/icons";

type ProfileProps = NativeStackScreenProps<RootStackParamList, "Pokemon">;
const PokemonScreen = ({ route, navigation }: ProfileProps) => {
  const baseUrl = "https://pokeapi.co/api/v2";
  const limit = 5;
  const [isLoading, setIsLoading] = useState(false);
  const [pageCurrent, setPageCurrent] = useState(0);

  useEffect(() => {
    console.log("useEffect ", pageCurrent);
    setIsLoading(true);
    fetchData();
  }, [pageCurrent]);

  const [data, setData] = useState([]);
  const [dataSort, setDataSort] = useState([
    {
      id: 1,
      text: "Smallest number first",
      status: 1,
      bgColor: styles.backgroundType_psychic.backgroundColor,
    },
    {
      id: 2,

      text: "Highest number first",
      status: 0,
      bgColor: styles.backgroundType_psychic.backgroundColor,
    },
    {
      id: 3,

      text: "A - Z",
      status: 0,
      bgColor: styles.backgroundType_psychic.backgroundColor,
    },
    {
      id: 4,

      text: "Z - A",
      status: 0,
      bgColor: styles.backgroundType_psychic.backgroundColor,
    },
  ]);
  const [dataGeneration, setDataGeneration] = useState([
    {
      id: 1,
      text: "Generation I",
      status: 1,
      bgColor: styles.backgroundType_psychic.backgroundColor,
      img1: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/1.png",
      img2: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/4.png",
      img3: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/7.png",
    },
    {
      id: 2,
      text: "Generation II",
      status: 0,
      bgColor: styles.backgroundType_psychic.backgroundColor,
      img1: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/152.png",
      img2: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/155.png",
      img3: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/158.png",
    },
    {
      id: 3,
      text: "Generation III",
      status: 0,
      bgColor: styles.backgroundType_psychic.backgroundColor,
      img1: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/252.png",
      img2: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/255.png",
      img3: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/258.png",
    },
    {
      id: 4,
      text: "Generation IV",
      status: 0,
      bgColor: styles.backgroundType_psychic.backgroundColor,
      img1: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/387.png",
      img2: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/390.png",
      img3: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/393.png",
    },
    {
      id: 5,
      text: "Generation V",
      status: 0,
      bgColor: styles.backgroundType_psychic.backgroundColor,
      img1: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/495.png",
      img2: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/498.png",
      img3: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/501.png",
    },
    {
      id: 6,
      text: "Generation VI",
      status: 0,
      bgColor: styles.backgroundType_psychic.backgroundColor,
      img1: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/650.png",
      img2: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/653.png",
      img3: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/656.png",
    },
    {
      id: 7,
      text: "Generation VII",
      status: 0,
      bgColor: styles.backgroundType_psychic.backgroundColor,
      img1: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/722.png",
      img2: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/725.png",
      img3: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/728.png",
    },
    {
      id: 8,
      text: "Generation VIII",
      status: 0,
      bgColor: styles.backgroundType_psychic.backgroundColor,
      img1: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/810.png",
      img2: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/813.png",
      img3: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/816.png",
    },
  ]);

  const [dataIcon, setDataIcon] = useState([
    { name: "bug", status: 0, bgColor: styles.colorType_bug.color },
    { name: "dark", status: 0, bgColor: styles.colorType_dark.color },
    { name: "dragon", status: 0, bgColor: styles.colorType_dragon.color },
    { name: "electric", status: 0, bgColor: styles.colorType_electric.color },
    { name: "fairy", status: 0, bgColor: styles.colorType_fairy.color },
    { name: "fighting", status: 0, bgColor: styles.colorType_fighting.color },
    { name: "fire", status: 0, bgColor: styles.colorType_fire.color },
    { name: "flying", status: 0, bgColor: styles.colorType_flying.color },
    { name: "ghost", status: 0, bgColor: styles.colorType_ghost.color },
    { name: "grass", status: 0, bgColor: styles.colorType_grass.color },
    { name: "ground", status: 0, bgColor: styles.colorType_ground.color },
    { name: "ice", status: 0, bgColor: styles.colorType_ice.color },
    { name: "normal", status: 0, bgColor: styles.colorType_normal.color },
    { name: "poison", status: 0, bgColor: styles.colorType_poison.color },
    { name: "psychic", status: 0, bgColor: styles.colorType_psychic.color },
    { name: "rock", status: 0, bgColor: styles.colorType_rock.color },
    { name: "steel", status: 0, bgColor: styles.colorType_steel.color },
    { name: "water", status: 0, bgColor: styles.colorType_water.color },
  ]);

  const [dataIconWeaknesses, setDataIconWeaknesses] = useState([
    { name: "bug", status: 0, bgColor: styles.colorType_bug.color },
    { name: "dark", status: 0, bgColor: styles.colorType_dark.color },
    { name: "dragon", status: 0, bgColor: styles.colorType_dragon.color },
    { name: "electric", status: 0, bgColor: styles.colorType_electric.color },
    { name: "fairy", status: 0, bgColor: styles.colorType_fairy.color },
    { name: "fighting", status: 0, bgColor: styles.colorType_fighting.color },
    { name: "fire", status: 0, bgColor: styles.colorType_fire.color },
    { name: "flying", status: 0, bgColor: styles.colorType_flying.color },
    { name: "ghost", status: 0, bgColor: styles.colorType_ghost.color },
    { name: "grass", status: 0, bgColor: styles.colorType_grass.color },
    { name: "ground", status: 0, bgColor: styles.colorType_ground.color },
    { name: "ice", status: 0, bgColor: styles.colorType_ice.color },
    { name: "normal", status: 0, bgColor: styles.colorType_normal.color },
    { name: "poison", status: 0, bgColor: styles.colorType_poison.color },
    { name: "psychic", status: 0, bgColor: styles.colorType_psychic.color },
    { name: "rock", status: 0, bgColor: styles.colorType_rock.color },
    { name: "steel", status: 0, bgColor: styles.colorType_steel.color },
    { name: "water", status: 0, bgColor: styles.colorType_water.color },
  ]);

  const [dataIconH, setDataIconH] = useState([
    { name: "short", status: 0, bgColor: styles.height_short.backgroundColor },
    {
      name: "medium",
      status: 0,
      bgColor: styles.height_medium.backgroundColor,
    },
    { name: "tall", status: 0, bgColor: styles.height_tall.backgroundColor },
  ]);

  const [dataIconW, setDataIconW] = useState([
    { name: "light", status: 0, bgColor: styles.weight_light.backgroundColor },
    {
      name: "normalw",
      status: 0,
      bgColor: styles.weight_normal.backgroundColor,
    },
    { name: "heavy", status: 0, bgColor: styles.weight_heavy.backgroundColor },
  ]);

  const [modalVisible, setModalVisible] = useState<boolean>(false);
  const [modalSortVisible, setModalSortVisible] = useState<boolean>(false);
  const [modalGenerationVisible, setModalGenerationVisible] =
    useState<boolean>(false);
  const getBGLabelColor = (type: string) => {
    switch (type) {
      case "bug":
        return styles.colorType_bug.color;
      case "dark":
        return styles.colorType_dark.color;
      case "dragon":
        return styles.colorType_dragon.color;
      case "electric":
        return styles.colorType_electric.color;
      case "fairy":
        return styles.colorType_fairy.color;
      case "fighting":
        return styles.colorType_fighting.color;
      case "fire":
        return styles.colorType_fire.color;
      case "flying":
        return styles.colorType_flying.color;
      case "ghost":
        return styles.colorType_ghost.color;
      case "grass":
        return styles.colorType_grass.color;
      case "ground":
        return styles.colorType_ground.color;
      case "ice":
        return styles.colorType_ice.color;
      case "normal":
        return styles.colorType_normal.color;
      case "poison":
        return styles.colorType_poison.color;
      case "psychic":
        return styles.colorType_psychic.color;
      case "rock":
        return styles.colorType_rock.color;
      case "steel":
        return styles.colorType_steel.color;
      case "water":
        return styles.colorType_water.color;
      default:
        return styles.colorType_grass.color;
    }
  };
  const getBGColor = (type: string) => {
    switch (type) {
      case "bug":
        return styles.backgroundType_bug;
      case "dark":
        return styles.backgroundType_dark;
      case "dragon":
        return styles.backgroundType_dragon;
      case "electric":
        return styles.backgroundType_electric;
      case "fairy":
        return styles.backgroundType_fairy;
      case "fighting":
        return styles.backgroundType_fighting;
      case "fire":
        return styles.backgroundType_fire;
      case "flying":
        return styles.backgroundType_flying;
      case "ghost":
        return styles.backgroundType_ghost;
      case "grass":
        return styles.backgroundType_grass;
      case "ground":
        return styles.backgroundType_ground;
      case "ice":
        return styles.backgroundType_ice;
      case "normal":
        return styles.backgroundType_normal;
      case "poison":
        return styles.backgroundType_poison;
      case "psychic":
        return styles.backgroundType_psychic;
      case "rock":
        return styles.backgroundType_rock;
      case "steel":
        return styles.backgroundType_steel;
      case "water":
        return styles.backgroundType_water;
      default:
        return styles.backgroundType_grass;
    }
  };
  const renderItem = ({ item }) => {
    return (
      <View style={{ paddingTop: 15 }}>
        <TouchableOpacity
          onPress={() => {
            console.log("press ", item.name);
            navigation.navigate("Profile");
          }}
          style={[
            getBGColor(item.types[0].type.name),
            {
              marginVertical: 5,
              paddingLeft: 20,
              paddingVertical: 20,
              borderRadius: 10,
              marginHorizontal: 20,
            },
          ]}
        >
          <Text>#001</Text>
          <Text style={{ color: "#FFFFFF", fontSize: 24, fontWeight: "700" }}>
            {item.name}
          </Text>
          <View style={{ flexDirection: "row" }}>
            {item.types.map((itemType, index) => (
              <View
                key={item.name + "" + index}
                style={[
                  styles.label,
                  {
                    backgroundColor: getBGLabelColor(itemType.type.name),
                    flexDirection: "row",
                  },
                ]}
              >
                {/* <Grass color={"#FFFFFF"} /> */}
                <Text style={[styles.text_white]}>
                  {" "}
                  {itemType.type.name.charAt(0).toUpperCase() +
                    itemType.type.name.slice(1)}
                </Text>
              </View>
            ))}
          </View>
          <Image
            style={{
              position: "absolute",
              resizeMode: "cover",
              alignSelf: "center",
            }}
            source={require("../../images/system/Pattern.png")}
          />
          <Image
            style={{
              position: "absolute",
              resizeMode: "cover",
              alignSelf: "flex-end",
            }}
            source={require("../../images/system/Pokeball2.png")}
          />
        </TouchableOpacity>
        <Image
          style={{
            position: "absolute",
            resizeMode: "contain",
            right: 25,
            width: 130,
            height: 130,
          }}
          source={{
            uri: item.uri,
          }}
          onLoadEnd={() => updateStatusImage(item.name)}
        />
        {item.status == 1 ? (
          <Image
            style={{
              position: "absolute",
              resizeMode: "contain",
              right: 25,
              width: 130,
              height: 130,
            }}
            source={require("../../images/system/loading.gif")}
          />
        ) : null}
      </View>
    );
  };

  const updateStatusImage = (name: string) => {
    console.log("done image ", name);
    let dataItem = [...data];
    dataItem.forEach(function (item) {
      if (item.name == name) {
        item.status = 0;
      }
    });
    setData(dataItem);
  };

  const fetchData = () => {
    let dataItem: any[] = [];
    axios({
      method: "get",
      url: `${baseUrl}/pokemon?limit=${limit}&offset=${limit * pageCurrent}`,
    }).then((response) => {
      // console.log(response.data.results);
      // console.log(response.data.results[0].url);
      // console.log(response.data.results);
      fetchImage(response.data.results, 0, dataItem);
    });
  };

  const fetchImage = (arrUrl: any, index: number, arrItem: any) => {
    let dataItem: any[] = arrItem;
    if (index < 5) {
      axios({
        method: "get",
        url: arrUrl[index].url,
      }).then((responseImage) => {
        dataItem.push({
          originalName: arrUrl[index].name,
          name:
            arrUrl[index].name.charAt(0).toUpperCase() +
            arrUrl[index].name.slice(1),
          bgColor: styles.backgroundType_grass,
          uri: responseImage.data.sprites.other["official-artwork"]
            .front_default,
          types: responseImage.data.types,
          status: 1,
        });
        fetchImage(arrUrl, index + 1, dataItem);
      });
    } else {
      setData(data.concat(arrItem));
      setIsLoading(false);
    }
  };

  const renderFooter = () => {
    return isLoading ? (
      <View>
        <ActivityIndicator />
      </View>
    ) : null;
  };

  const handleLoadMore = () => {
    console.log("handleLoadMore");
    console.log("isLoading ", isLoading);
    if (!isLoading) {
      setPageCurrent(pageCurrent + 1);
      setIsLoading(true);
    }
  };

  const updateText = (text) => {
    console.log(text);
  };

  const onCallBack = (data) => {
    console.log("callback ", data);
    setModalVisible(data);
  };
  const onCallBackSort = (data) => {
    console.log("callback ", data);
    setModalSortVisible(data);
  };
  const onCallBackGeneration = (data) => {
    console.log("callback ", data);
    setModalGenerationVisible(data);
  };

  return (
    <>
      {/* <Headers goBack={() => navigation.goBack()} /> */}
      <SafeAreaView style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
        <StatusBar barStyle={"dark-content"} />
        <View style={{}}>
          <Image
            style={{
              position: "absolute",
              bottom: 0,
              top: 0,
              left: 0,
              right: 0,
              alignItems: "center",
              justifyContent: "center",
              opacity: 0.5,
              width: "100%",
            }}
            source={require("../../images/system/Pokeball.png")}
          />
          <View
            style={{
              top: 10,
              right: 30,
              justifyContent: "flex-end",
              opacity: 0.5,
              width: "100%",
              flexDirection: "row",
            }}
          >
            <TouchableOpacity
              onPress={() => setModalGenerationVisible(!modalGenerationVisible)}
              style={{ padding: 10 }}
            >
              <Generation color={"black"} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setModalSortVisible(!modalSortVisible)}
              style={{ padding: 10 }}
            >
              <Sort color={"black"} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setModalVisible(!modalVisible)}
              style={{ padding: 10 }}
            >
              <Filter color={"black"} />
            </TouchableOpacity>
          </View>
          <View style={{ paddingTop: 60, paddingHorizontal: 20 }}>
            <View>
              <Text style={{ fontSize: 35, fontWeight: "bold" }}>
                Pokemon current page:{pageCurrent + ""}
              </Text>
            </View>
            <View style={{ marginTop: 10 }}>
              <Text style={styles.text_gray}>
                Search for Pokémon by name or using the National Pokédex number.
              </Text>
            </View>
            <View style={{ marginVertical: 20 }}>
              <TextInput
                placeholder="What Pokémon are you looking for?"
                style={[styles.background_defaultInput, styles.textField]}
                returnKeyType="done"
                onSubmitEditing={(event) => {
                  setData([]);
                  updateText(event.nativeEvent.text);
                }}
              ></TextInput>
            </View>
            <ModalFilter
              modalVisible={modalVisible}
              dataIcon={dataIcon}
              dataIconH={dataIconH}
              dataIconW={dataIconW}
              dataIconWeaknesses={dataIconWeaknesses}
              callBack={(data) => onCallBack(data)}
            />
            <ModalSort
              modalVisible={modalSortVisible}
              dataItem={dataSort}
              callBack={(data) => onCallBackSort(data)}
            />
            <ModalGeneration
              modalVisible={modalGenerationVisible}
              dataItem={dataGeneration}
              callBack={(data) => onCallBackGeneration(data)}
            />
          </View>
        </View>
        <FlatList
          data={data}
          renderItem={renderItem}
          keyExtractor={(item, index) => String("item" + index)}
          // stickyHeaderIndices={[0]}
          onEndReachedThreshold={0}
          onEndReached={({ distanceFromEnd }) => {
            handleLoadMore();
          }}
          ListFooterComponent={renderFooter}
        />
      </SafeAreaView>
    </>
  );
};

export default PokemonScreen;
