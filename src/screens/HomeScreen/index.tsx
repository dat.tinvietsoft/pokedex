import { NativeStackScreenProps } from "@react-navigation/native-stack";
import React, { useEffect, useContext, useState, useRef } from "react";
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TouchableOpacity,
  Image,
  KeyboardAvoidingView,
  Keyboard,
  TouchableWithoutFeedback,
  TextInput,
  ScrollView,
  FlatList,
  PixelRatio,
} from "react-native";
import { RootStackParamList } from "../../../App";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { useDispatch, useSelector } from "react-redux";
import styles from "../../themes/global";

type ProfileProps = NativeStackScreenProps<RootStackParamList, "Home">;
const HomeScreen = ({ route, navigation }: ProfileProps) => {
  const data = [
    {
      name: "Pokedex",
      bgColor: styles.backgroundType_psychic,
      action: "Pokedex",
    },
    {
      name: "Pokemon",
      bgColor: styles.backgroundType_grass,
      action: "Pokemon",
    },
    {
      name: "Moves",
      bgColor: styles.backgroundType_fire,
    },
    {
      name: "Abilities",
      bgColor: styles.backgroundType_water,
    },
    {
      name: "Items",
      bgColor: styles.backgroundType_rock,
    },
    {
      name: "Regions",
      bgColor: styles.backgroundType_steel,
    },
    {
      name: "Locations",
      bgColor: styles.backgroundType_flying,
    },
    {
      name: "Eggs",
      bgColor: styles.backgroundType_ghost,
    },
    {
      name: "Types",
      bgColor: styles.backgroundType_dragon,
    },
  ];
  const renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => {
          console.log("press ", item.name);
          navigation.navigate(item.action);
        }}
        style={[
          { ...item.bgColor },
          {
            marginVertical: 10,
            paddingLeft: 20,
            paddingVertical: 40,
            borderRadius: 10,
            marginHorizontal: 20,
          },
        ]}
      >
        <Text style={{ color: "#FFFFFF", fontSize: 24, fontWeight: "700" }}>
          {item.name}
        </Text>
        <Image
          style={{
            position: "absolute",
            resizeMode: "cover",
            alignSelf: "center",
          }}
          source={require("../../images/system/Pattern.png")}
        />
        <Image
          style={{
            position: "absolute",
            resizeMode: "cover",
            alignSelf: "flex-end",
          }}
          source={require("../../images/system/Pokeball2.png")}
        />
      </TouchableOpacity>
    );
  };
  return (
    <>
      <SafeAreaView style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
        <StatusBar barStyle={"dark-content"} />
        <View style={{}}>
          <Image
            style={{
              position: "absolute",
              bottom: 0,
              top: 0,
              left: 0,
              right: 0,
              alignItems: "center",
              justifyContent: "center",
              opacity: 0.5,
              width: "100%",
            }}
            source={require("../../images/system/Pokeball.png")}
          />
          <View style={{ paddingTop: 60 }}>
            <View style={{ padding: 20 }}>
              <Text style={{ fontSize: 35, fontWeight: "bold" }}>Category</Text>
            </View>
          </View>
        </View>
        <FlatList
          data={data}
          renderItem={renderItem}
          keyExtractor={(item, index) => String(index)}
        />
      </SafeAreaView>
    </>
  );
};

export default HomeScreen;
