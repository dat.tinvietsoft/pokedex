import React from "react";
import styles from "../../themes/global";
import { StyleProp, Text, TextStyle, View } from "react-native";

type Props = {
  icon: JSX.Element;
  text?: string;
  color: StyleProp<TextStyle>;
};

const Badge = ({ icon, text, color }: Props) => {
  return (
    <View
      style={[
        {
          borderRadius: 3,
          padding: 5,
          gap: 5,
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
        },
        color,
      ]}
    >
      {icon}
      {text ? (
        <Text style={[styles.fontPokemonType, styles.text_white]}>{text}</Text>
      ) : null}
    </View>
  );
};

export default Badge;
