import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  SafeAreaView,
  StatusBar,
  NativeModules,
  Platform,
} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
const { StatusBarManager } = NativeModules;

const Headers = ({
  goBack,
  text,
  hiddenBack = false,
  hiddenRight = false,
  color = "black",
}) => {
  const STATUSBAR_HEIGHT = Platform.OS === "ios" ? 20 : StatusBarManager.HEIGHT;
  const windowHeight = Dimensions.get("window").height;
  console.log(STATUSBAR_HEIGHT);
  return (
    <>
      <View style={{ position: "relative", top: STATUSBAR_HEIGHT }}>
        <TouchableOpacity onPress={goBack} style={{ padding: 10 }}>
          <Icon name={"chevron-left"} size={30} color={color} />
        </TouchableOpacity>
      </View>
    </>
  );
};

export default Headers;
